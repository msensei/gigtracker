from __future__ import unicode_literals

from django.db import models

from benefit.models import BenefitPackage
from common.constants import STANDARD_DECIMAL_PLACES, \
    STANDARD_DECIMAL_MAX_DIGITS, STANDARD_CHARFIELD_LENGTH
from common.models import DefaultFieldsMixin, DescriptionMixin
from gig.models import Gig
from tag.models import Tag


class Score(models.Model):
    """A score given to an item."""
    score = models.IntegerField(unique=True)
    # color = models.CharField(max_length=STANDARD_CHARFIELD_LENGTH)


# class ScoreAttribute(models.Model):
#     score = models.ForeignKey(Score)
#     color = models.CharField(max_length=STANDARD_CHARFIELD_LENGTH)


class ScoreSchema(DefaultFieldsMixin, DescriptionMixin):
    """A set of score to tag relationships."""
    # uuid =
    pass


class ScoreSchemaMixin(models.Model):
    """Mixing for including both score and score schema."""
    schema = models.ForeignKey(ScoreSchema)
    score = models.ForeignKey(Score)

    class Meta(object):
        abstract = True
        unique_together = ['schema', 'score']


class TagScore(ScoreSchemaMixin):
    tag = models.ForeignKey(Tag)


class BenefitScore(ScoreSchemaMixin):
    benefit_package = models.ForeignKey(BenefitPackage)


class SalaryScore(ScoreSchemaMixin):
    """The hourly salary scores."""
    rate_lower = models.DecimalField(max_digits=STANDARD_DECIMAL_MAX_DIGITS,
                                     decimal_places=STANDARD_DECIMAL_PLACES)
    rate_upper = models.DecimalField(max_digits=STANDARD_DECIMAL_MAX_DIGITS,
                                     decimal_places=STANDARD_DECIMAL_PLACES)


class GigScore(ScoreSchemaMixin):
    """Cached scores of gigs."""
    gig = models.ForeignKey(Gig)
