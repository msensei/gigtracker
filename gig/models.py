from __future__ import unicode_literals

from django.db import models

from common.constants import STANDARD_DECIMAL_MAX_DIGITS, \
    STANDARD_DECIMAL_PLACES, STANDARD_CHARFIELD_LENGTH
from common.models import DefaultFieldsMixin, DescriptionMixin
from location.models import Location
from tag.models import Tag


class Gig(DefaultFieldsMixin, DescriptionMixin):
    """Gig data model."""
    tags = models.ManyToManyField(Tag)
    location = models.ForeignKey(Location)


# class GiggSalary(models.Model):
#     salary = models.DecimalField(max_digits=STANDARD_DECIMAL_MAX_DIGITS,
#                                  decimal_places=STANDARD_DECIMAL_PLACES)


class GigPackage(models.Model):
    #salary
    pass
