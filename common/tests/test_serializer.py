"""JSON Serializer tests."""
from django.test import TestCase

from common.serializer import json_to_python
from common.serializer import python_to_json


class JSONSerializerTest(TestCase):
    """Tests designed to evaluate efficacy of the JSON serializer."""

    def test_asset_serializer_returns_json(self):
        """Simple serializer test."""
        test_data = [{'foo': 'bar'}]
        expected_results = '[{"foo": "bar"}]'
        self.assertJSONEqual(python_to_json(test_data), expected_results)

    def test_asset_deserializer_returns_python(self):
        """Simple deserializer test."""
        expected_results = [{'foo': 'bar'}]
        test_data = '[{"foo": "bar"}]'
        self.assertEqual(json_to_python(test_data), expected_results)
