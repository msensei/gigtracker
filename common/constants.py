"""Common constants used throughout the application."""

__author__ = ('Michael Clark (michael.clark2@igt.com)',)

CREATED_KEY = 'created'
DESCRIPTION_KEY = 'description'
HTML_MIME_TYPE = 'text/html; charset=utf-8'
JSON_MIME_TYPE = 'application/json'
MODIFIED_KEY = 'modified'
NAME_KEY = 'name'
PK_KEY = 'pk'
STANDARD_CHARFIELD_LENGTH = 254
STANDARD_DECIMAL_MAX_DIGITS = 10
STANDARD_DECIMAL_PLACES = 2
UID_KEY = 'UID'
USER_KEY = 'user'
