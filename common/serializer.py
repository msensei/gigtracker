"""JSON Tools for managing JSON data."""
import json

from django.core.serializers.json import DjangoJSONEncoder


def python_to_json(python_object):
    """Convert a Python object to a JSON string, using the Django encoder.

    Args:
        python_object: A Python string, list, or dictionary.

    Returns:
        A JSON string equivalent of python_object.
    """
    return json.dumps(python_object, cls=DjangoJSONEncoder, sort_keys=True)


def json_to_python(json_string):
    """Convert a JSON string to a Python object.

    Args:
        json_string: A JSON string, equivalent to a Python object.

    Returns:
        A Python object (dict).
    """
    return json.loads(json_string)
