"""Common model patterns for the Bingo application."""

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from common.constants import STANDARD_CHARFIELD_LENGTH


# pylint: disable=too-few-public-methods


class CreatedMixin(models.Model):
    """Adds an auto populated created field to a model."""
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta(object):
        """Standard Django Meta object, for model configuration."""
        abstract = True


class DescriptionMixin(models.Model):
    """Adds a standard sized description field to a model."""
    description = models.CharField(max_length=STANDARD_CHARFIELD_LENGTH,
                                   blank=True, null=True)

    class Meta(object):
        """Standard Django Meta object, for model configuration."""
        abstract = True


class ModifiedMixin(models.Model):
    """Adds an auto populated modified field to a model."""
    modified = models.DateTimeField(auto_now=True, db_index=True)

    class Meta(object):
        """Standard Django Meta object, for model configuration."""
        abstract = True


@python_2_unicode_compatible
class NameMixin(models.Model):
    """Adds standard length name to a model, and makes name typical output."""
    name = models.CharField(max_length=STANDARD_CHARFIELD_LENGTH,
                            db_index=True, unique=True)

    class Meta(object):
        """Standard Django Meta object, for model configuration."""
        abstract = True

    def __str__(self):
        """Simple default helper for objects with the NameMixin."""
        return self.name

    def natural_key(self):
        """Shows 'name' instead of PK when serializing with natural_keys."""
        return self.name


class DefaultFieldsMixin(CreatedMixin, ModifiedMixin, NameMixin):
    """A model which contains the typical startup fields for a model."""

    class Meta(object):
        """Standard Django Meta object, for model configuration."""
        abstract = True


class Timeframe(NameMixin, DescriptionMixin):
    pass
