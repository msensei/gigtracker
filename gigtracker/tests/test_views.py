"""Base View Tests for GigTracker."""
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client

from common.constants import HTML_MIME_TYPE


class MainViewsTest(TestCase):
    """Test main views are serving."""
    def setUp(self):
        self.client = Client()

    def tearDown(self):
        self.client = None

    def test_index_view_serves(self):
        """Assert Index View servers proper templates and values."""
        test_request = self.client
        response = self.client.get(reverse('index'),
                                   follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('Content-Type'),
                         HTML_MIME_TYPE)
        self.assertTemplateUsed(response, 'index.html')
