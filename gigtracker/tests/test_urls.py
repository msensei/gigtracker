"""Base URL Tests for GigTracker."""
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client

from common.constants import HTML_MIME_TYPE


class MainURLsTest(TestCase):
    """Test main urls are serving."""
    def setUp(self):
        self.client = Client()

    def tearDown(self):
        self.client = None

    def test_index_url_reverses(self):
        """Assert Index View servers proper templates and values."""
        response = reverse('index')
        self.assertResponse = '/'
