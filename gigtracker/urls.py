"""gigtracker URL Configuration."""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView

from gigtracker.views import index_view, agile_view

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index_view, name='index'),
    url(r'^agile/', agile_view),
]
