
[uwsgi]
chdir=/gigtracker  # Start in this directory.
chmod-socket=666  # CHMOD the socket file to 666.
daemonize=/var/log/uwsgi/gigtracker.log  # Log file.
gid = uwsgi  # Group to run the process in.
master = True  # This launches as a 'master' daemon to run the workers.
max-requests = 5000  # Reload workers after N requests.
module = gigtracker.wsgi:application  # The WSGI module to load.
pidfile = /tmp/gigtracker.pid  # Where to put the PID file for master process.
procname = uwsgi_gigtracker  # What to name the running processes.
setenv DJANGO_SETTINGS_MODULE=gigtracker.settings;  # The Django settings.
# socket can be a file, or an IP:port. Use http instead to view normally.
socket = /tmp/gigtracker.sock  # The socket file (for NGINX).
stats = 0.0.0.0:9091  # Runs the 'stats' module for monitoring uWSGI.
threads = 3  # The number of threads per process (worker).
touch-reload=/gigtracker/gigtracker/system/uwsgi.ini  # Touch to reload.
uid = uwsgi  # The user to run the process as.
vacuum = True  # When finished, remove all created files.
virtualenv = /home/uwsgi/venv/  # The Python virtualenv location.
workers = 5  # The number of processes (workers) to launch.
