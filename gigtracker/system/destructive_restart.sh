#!/usr/bin/env bash
set -x

APP_NAME='gigtracker'
APP_LOG='/var/log/$APP_NAME'
APP_ROOT='/'$APP_NAME
APP_CONF=$APP_ROOT'/'$APP_NAME
GIT_ROOT='/usr/src/'$APP_NAME
NGINX_CONF_D='/etc/nginx/conf.d'
APP_SYSTEM=$APP_CONF'/system'
VIRTENV='/home/uwsgi/venv'
VIRTENV_ACTIVATE=$VIRTENV'/bin/activate'


# STOP ALL
echo '#####################STOP'
source $VIRTENV_ACTIVATE
/home/uwsgi/venv/bin/uwsgi --stop /tmp/$APP_NAME.pid || true
deactivate
killall -9 uwsgi || true

sudo service nginx stop

# NUKE ROOT
echo '#####################NUKE'
#sudo -s #USER: ROOT
rm /var/log/nginx/error.log || true
sudo rm /var/log/uwsgi/$APP_NAME.log || true
rm $NGINX_CONF_D/$APP_NAME.conf || true
rm $APP_LOG

rm -rf $APP_ROOT || true
#mkdir $APP_ROOT
#chmod a+r,a+x $APP_ROOT
#chmod -R u=,g=w,o=rwx $APP_ROOT


# CLONE
echo '#####################CLONE'
# git fetch
cp -r $GIT_ROOT $APP_ROOT

# LINK NGINX
sudo ln -s $APP_SYSTEM/nginx.conf $NGINX_CONF_D/$APP_NAME.conf

# START UWSGI
echo '#####################START'
# sudo -u uwsgi -i
sudo chmod -R u=,g=w,o=rwx $APP_ROOT
source $VIRTENV_ACTIVATE
/home/uwsgi/venv/bin/uwsgi --ini $APP_SYSTEM/uwsgi.ini
deactivate

sudo service nginx start

echo '#####################STATUS'
# SYSTEM OVERVIEW
ps aux | grep nginx
ps aux | grep uwsgi

# LOG REVIEW
sudo chmod 777 /var/log/nginx/error.log
cat /var/log/nginx/error.log
sudo chmod 777 /var/log/uwsgi/$APP_NAME.log
cat /var/log/uwsgi/$APP_NAME.log

# exit #USER: ROOT

