#!/usr/bin/env bash

APP_NAME='gigtracker'
APP_ROOT='/'$APP_NAME
APP_CONF=$APP_ROOT'/'$APP_NAME
GIT_ROOT='/usr/src/'$APP_NAME
NGINX_CONF_D='/etc/nginx/conf.d'
APP_SYSTEM=$APP_CONF'/system'
VIRTENV='/home/uwsgi/venv'
VIRTENV_ACTIVATE=$VIRTENV'/bin/activate'
