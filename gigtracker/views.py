"""Main Views for gigtracker."""
from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = 'index.html'


class AgileView(TemplateView):
    template_name = 'agile.html'


agile_view = AgileView.as_view()
index_view = IndexView.as_view()
