"""The remote system configuration variables.

GIT_URL = The url to use for git fetch.
NGINX_CONF = This is attached to '/etc/nginx/conf.d'
REQUIREMENTS_TXT = The file run by pip -r requirements.txt remotely.
UWSGI_APP = The uwsgi application used.
UWSGI_DJANGO_ROOT = The application home folder on the remote.
UWSGI_USER = The user to launch with "su $UWSGI_USER;"
UWSGI_INI = The uwsgi.ini to use when the app is run.
UWSGI_VIRTENV = The Python virtual environment to use when executing.
"""
GIT_URL = 'https://msensei@bitbucket.org/msensei/gigtracker.git'
NGINX_CONF = '/gigtracker/gigtracker/system/nginx.conf'
REQUIREMENTS_TXT = '/gigtracker/gigtracker/system/requirements.txt'
SSH_HOSTS = ['54.183.195.66']
SSH_PEM = '/Users/BIT/aws_clark_001.pem'
SSH_USER = 'ec2-user'
UWSGI_DJANGO_ROOT = '/gigtracker'
UWSGI_INI = '/gigtracker/gigtracker/system/uwsgi.ini'
UWSGI_USER = 'uwsgi'
UWSGI_VIRTENV = '/home/uwsgi/venv/'
UWSGI_APP = '%sbin/uwsgi' % UWSGI_VIRTENV
