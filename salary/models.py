from __future__ import unicode_literals

from django.db import models

from common.constants import STANDARD_DECIMAL_MAX_DIGITS, \
    STANDARD_DECIMAL_PLACES
from common.models import NameMixin, Timeframe


class Salary(models.Model):
    rate = models.DecimalField(max_digits=STANDARD_DECIMAL_MAX_DIGITS,
                               decimal_places=STANDARD_DECIMAL_PLACES)
    timeframe = models.ForeignKey(Timeframe)

