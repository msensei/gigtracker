"""Test the basic model structure of the company models."""

from django.test import TestCase

from common import constants
from company.models import Company


class CoreCompanyModelsTests(TestCase):
    """Test core functionality of Company models."""
    def test_company_name_in_model(self):
        """Company name in model."""
        test_name = 'my company name'
        test_company = Company.objects.create(name=test_name)
        result = Company.objects.get(id=test_company.id)
        self.assertEqual(result.name, test_name)

    def test_description_in_model(self):
        """Company description in model."""
        test_description = 'my company description'
        test_company = Company.objects.create(description=test_description)
        result = Company.objects.get(id=test_company.id)
        self.assertEqual(result.description, test_description)

    def test_company_fields(self):
        """Assert expected fields are present."""
        test_company = Company.objects.create()
        self.assertTrue(hasattr(test_company, constants.NAME_KEY))
        self.assertTrue(hasattr(test_company, constants.DESCRIPTION_KEY))
        self.assertTrue(hasattr(test_company, constants.CREATED_KEY))
        self.assertTrue(hasattr(test_company, constants.MODIFIED_KEY))
