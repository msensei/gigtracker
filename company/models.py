from __future__ import unicode_literals

from django.db import models

from common.models import DefaultFieldsMixin, DescriptionMixin
from location.models import Location


class Company(DefaultFieldsMixin, DescriptionMixin):
    """Main Company data model."""
    location = models.ForeignKey(Location, blank=True, null=True)
