from __future__ import unicode_literals

from django.apps import AppConfig


class DjangoStackerConfig(AppConfig):
    name = 'django_stacker'
