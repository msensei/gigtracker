"""Django Stacker ps aux."""
from django.core.management.base import BaseCommand
from fabric.api import execute

from django_stacker.fab_modules.commands import ps_aux
from django_stacker.fab_modules import base_env


class Command(BaseCommand):
    help = 'Fetch a ps aux from the remote system.'
    can_import_settings = True

    def handle(self, *args, **kwargs):
        execute(ps_aux)
