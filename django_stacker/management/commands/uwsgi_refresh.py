"""Django Stacker command integrations."""
from django.core.management.base import BaseCommand
from fabric.api import execute

from django_stacker.fab_modules.commands import uwsgi_refresh
from django_stacker.fab_modules import base_env


class Command(BaseCommand):
    help = 'Restarts uwsgi workers after code changes.'
    can_import_settings = True

    def handle(self, *args, **kwargs):
        execute(uwsgi_refresh)
