"""Test the stacker command integrations."""
from django.core.management import call_command
from django.test import TestCase
from django.utils.six import StringIO
from mock import patch


class ManagementCommandTests(TestCase):
    """Assert tests that manage.py commands actually execute."""

    @patch('django_stacker.management.commands.uwsgi_refresh.uwsgi_refresh')
    def test_command_output(self, uwsgi_refresh):
        """Assert first commands executes."""
        out = StringIO()
        call_command('uwsgi_refresh', stdout=out)
        self.assertIn('', out.getvalue())
        uwsgi_refresh.assert_called_once_with()

    @patch('django_stacker.management.commands.ps_aux.ps_aux')
    def test_command_output(self, uwsgi_refresh):
        """Assert first commands executes."""
        out = StringIO()
        call_command('uwsgi_refresh', stdout=out)
        self.assertIn('', out.getvalue())
        uwsgi_refresh.assert_called_once_with()

    @patch('django_stacker.management.commands.uwsgi_start.uwsgi_start')
    def test_command_output(self, uwsgi_refresh):
        """Assert first commands executes."""
        out = StringIO()
        call_command('uwsgi_start', stdout=out)
        self.assertIn('', out.getvalue())
        uwsgi_refresh.assert_called_once_with()

    @patch('django_stacker.management.commands.uwsgi_stats.uwsgi_stats')
    def test_command_output(self, uwsgi_refresh):
        """Assert first commands executes."""
        out = StringIO()
        call_command('uwsgi_stats', stdout=out)
        self.assertIn('', out.getvalue())
        uwsgi_refresh.assert_called_once_with()

    @patch('django_stacker.management.commands.uwsgi_status.uwsgi_status')
    def test_command_output(self, uwsgi_refresh):
        """Assert first commands executes."""
        out = StringIO()
        call_command('uwsgi_status', stdout=out)
        self.assertIn('', out.getvalue())
        uwsgi_refresh.assert_called_once_with()

    @patch('django_stacker.management.commands.uwsgi_stop.uwsgi_stop')
    def test_command_output(self, uwsgi_refresh):
        """Assert first commands executes."""
        out = StringIO()
        call_command('uwsgi_stop', stdout=out)
        self.assertIn('', out.getvalue())
        uwsgi_refresh.assert_called_once_with()
