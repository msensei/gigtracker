"""Django Stacker command integrations uWSGI Start."""
from django.core.management.base import BaseCommand
from fabric.api import execute

from django_stacker.fab_modules.commands import uwsgi_stop
from django_stacker.fab_modules import base_env


class Command(BaseCommand):
    help = 'Stop uWSGI workers.'
    can_import_settings = True

    def handle(self, *args, **kwargs):
        execute(uwsgi_stop)
