"""Django Stacker command integrations uWSGI Log Aggregations."""
from django.core.management.base import BaseCommand
from fabric.api import execute

from django_stacker.fab_modules.commands import uwsgi_stats
from django_stacker.fab_modules import base_env


class Command(BaseCommand):
    help = 'Aggregations from uWSGI log file.'
    can_import_settings = True

    def handle(self, *args, **kwargs):
        execute(uwsgi_stats)
