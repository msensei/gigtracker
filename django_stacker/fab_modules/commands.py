"""Root file containing base objects and exported public commands.

Anything not prefixed with a "_" will be displayed in the fab help.
"""
from fabric.context_managers import quiet

from django_stacker.fab_modules.constants import PROCESS_ATTRIBUTE_LIST
from django_stacker.fab_modules.uwsgi import UwsgiManager as _UwsgiManager
from django_stacker.fab_modules.common import pretty_list_dict as _pretty
from django_stacker.fab_modules.process import ps_aux as _process_ps_aux


def uwsgi_start():
    """Start uWSGI on the remote server(s)."""
    uwsgi_manager = _UwsgiManager()
    uwsgi_manager.start()


def uwsgi_stop():
    """Stop uWSGI on the remote server(s)."""
    uwsgi_manager = _UwsgiManager()
    uwsgi_manager.stop()


def uwsgi_stats():
    """A brief synopsis of the existing log file."""
    uwsgi_manager = _UwsgiManager()
    uwsgi_manager.aggregations()


def uwsgi_status():
    """A check for "up" using the expected remote process details."""
    uwsgi_manager = _UwsgiManager()
    uwsgi_manager.status()


def uwsgi_refresh():
    """Pull the desired branch to the server, touch restart, pid change."""
    uwsgi_manager = _UwsgiManager()
    uwsgi_manager.refresh()


def ps_aux():
    """Display the dictionary of remote processes."""
    with quiet():
        process_list = _process_ps_aux()
    print _pretty(process_list,
                  header_list=PROCESS_ATTRIBUTE_LIST)

