"""Import the django settings into the Fabric environment."""
from django.conf import settings
from fabric.api import env

env.hosts = settings.SSH_HOSTS
env.user = settings.SSH_USER
env.key_filename = settings.SSH_PEM
