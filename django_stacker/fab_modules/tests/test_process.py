from unittest import TestCase

from mock import patch

from django_stacker.fab_modules.constants import PROCESS_ATTRIBUTE_LIST
from django_stacker.fab_modules.process import fetch_process_dict


class ProcessTests(TestCase):
    """Assert process dictionary processes correctly."""

    @patch('django_stacker.fab_modules.common.run')
    def test_fetch_process_dictionary(self, common_run):
        """Assert dictionary is reqturned from process."""
        return_value = """{'username': 'uwsgi', 'status':
        'sleeping', 'num_threads': 1, 'name': 'uwsgi', 'cpu_percent': 0.0,
        'pid': 10614, 'cmdline': ['uwsgi_gigtracker'], 'memory_percent':
        2.9780705712480824, 'create_time': 1457121305.93}"""

        class ResultClass(object):
            def __str__(self):
                return return_value

            return_code = 0

        common_run.return_value = ResultClass()

        test_pid = 123
        result = fetch_process_dict(test_pid)
        expected_result = (
            'python -c "from psutil import Process;'
            'print Process(%s).as_dict(attrs=%s);"' % (
                test_pid,
                PROCESS_ATTRIBUTE_LIST))
        common_run.assert_called_with(expected_result)
        self.assertTrue(common_run.called)
        expected = {u'username': u'uwsgi', u'status': u'sleeping',
                    u'num_threads': 1, u'name': u'uwsgi', u'cpu_percent': 0.0,
                    u'memory_percent': 2.9780705712480824, u'pid': 10614,
                    u'cmdline': u'uwsgi_gigtracker',
                    u'create_time': '2016-03-04 11:55:05'}
        self.assertDictEqual(result, expected)
