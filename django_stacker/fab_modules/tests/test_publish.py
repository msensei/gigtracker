"""Tests for publish module."""

from django.test import TestCase
from mock import patch

from django_stacker.fab_modules.publish import pull_migrate_and_collect


class PubishTests(TestCase):
    """Simiple tests that the correct sudo is being called."""

    @patch('django_stacker.fab_modules.publish.git_pull')
    @patch('django_stacker.fab_modules.publish.migrate')
    @patch('django_stacker.fab_modules.publish.collect_static')
    def test_pull_migrate_and_collect_calls_appropriate_functions(self,
                                                                  collect_static,
                                                                  migrate,
                                                                  git_pull):
        """Assert pull_migrate_and_collect calls proper functions."""
        pull_migrate_and_collect()
        collect_static.assert_called_once_with()
        migrate.assert_called_once_with()
        git_pull.assert_called_once_with()

    @patch('django_stacker.fab_modules.publish.settings')
    @patch('django_stacker.fab_modules.publish.cd')
    @patch('django_stacker.fab_modules.publish.prefix')
    @patch('django_stacker.fab_modules.publish.sudo')
    def test_migrate_calls_sudo(self, publish_settings, cd, prefix, sudo):
        """Assert sudo command called correctly given the variables."""
        publish_settings.assert_called_once_with()
        cd.assert_called_once_with()
        prefix.assert_called_once_with()
        sudo.assert_called_once_with()
