import logging
from unittest import TestCase

from mock import MagicMock
from mock import patch

from django_stacker.fab_modules.common import chown, chmod, python_c

TEST_CHMOD_RESULTS = """bla"""


class CommonFabScriptTests(TestCase):
    """Simple tests of the basic commands in this fab build."""

    @patch('django_stacker.fab_modules.common.run')
    @patch('django_stacker.fab_modules.common.quiet')
    @patch('django_stacker.fab_modules.common.sudo')
    def test_chown_parsed_result(self, common_sudo, common_quiet,
                                       common_run):
        """Assert owner change is executed."""
        test_username = 'test'
        test_groupname = 'tgroup'
        test_pathname = '/mypath'
        result = chown(test_pathname, test_username, test_groupname)
        expected_result = 'chown -R %s:%s %s' % (
        test_username, test_groupname, test_pathname)
        common_sudo.assert_called_with(expected_result)
        self.assertTrue(common_quiet.called)
        self.assertTrue(common_sudo.called)
        self.assertFalse(common_run.called)
        self.assertEqual(result, None)


    @patch('django_stacker.fab_modules.common.run')
    @patch('django_stacker.fab_modules.common.quiet')
    @patch('django_stacker.fab_modules.common.sudo')
    def test_permissions_parsed_result(self, common_sudo, common_quiet,
                                       common_run):
        """Assert permissions change executed."""
        test_pathname = '/mypath'
        test_permissions = '777'
        result = chmod(test_permissions, test_pathname)
        expected_result = 'chmod -R 777 %s' % (test_pathname)
        common_sudo.assert_called_with(expected_result)
        self.assertTrue(common_quiet.called)
        self.assertTrue(common_sudo.called)
        self.assertFalse(common_run.called)
        self.assertEqual(result, None)

    @patch('django_stacker.fab_modules.common.run')
    def test_python_c_calls_correct_string(self, common_run):
        """Assert run is called with python -c ..."""
        run_result = MagicMock()
        run_result.return_code = 0
        common_run.return_value = run_result
        test_python_string = 'test'
        expected_call = 'python -c "test"'
        result = python_c(test_python_string)
        common_run.assert_called_with(expected_call)
        self.assertEqual(result, run_result)

    @patch('django_stacker.fab_modules.common.run')
    def test_python_c_calls_failed_string(self, common_run):
        """Assert run is called with python -c ..."""
        run_result = MagicMock()
        run_result.return_code = 1
        common_run.return_value = run_result
        test_python_string = 'test'
        expected_call = 'python -c "test"'
        result = python_c(test_python_string)
        common_run.assert_called_with(expected_call)
        self.assertEqual(result, None)
