"""Test runner for uWSGI process fabric."""
from StringIO import StringIO
from unittest import TestCase

from mock import ANY
from mock import patch

from django_stacker.fab_modules.common import logger as common_logger
from django_stacker.fab_modules.constants import REQUEST_KEY, WORKER_KEY, \
    DAEMONIZE, CMDLINE, WORKERS, PROCNAME, UID, PIDFILE
from django_stacker.fab_modules.uwsgi import UwsgiLogDict, \
    uwsgi_worker_processes, UwsgiINI, UwsgiManager, logger

EXAMPLE_LOG_LINE = (
    "[pid: 12256|app: 0|req: 1/1] 23.242.203.246 () {44 vars in 754 bytes} ["
    "Fri "
    "Mar  4 21:04:21 2016] GET / => generated 4958 bytes in 20 msecs (HTTP/1.1 "
    "200) 2 headers in 88 bytes (1 switches on core 0)")

EXAMPLE_MASTER_LINE = """gracefully (RE)spawned uWSGI master process (pid:
10614)"""
EXAMPLE_CHILD_LINE_0 = """spawned uWSGI worker 1 (pid: 12254, cores: 3)"""
EXAMPLE_CHILD_LINE_1 = """spawned uWSGI worker 2 (pid: 12255, cores: 3)"""
EXAMPLE_CHILD_LINE_2 = """spawned uWSGI worker 1 (pid: 12256, cores: 3)"""
EXAMPLE_CHILD_LINE_3 = """spawned uWSGI worker 2 (pid: 12257, cores: 3)"""


logger.setLevel('ERROR')
common_logger.setLevel('ERROR')


class TestUWsgiLogParser(TestCase):
    """Test the parser for uwsgi."""

    def test_parse_object(self):
        """Assert a single line is parsed properly."""
        parser = UwsgiLogDict()
        test_string_io = StringIO()
        test_string_io.writelines([EXAMPLE_LOG_LINE])
        test_string_io.seek(0)
        parser.parse_file(test_string_io)
        self.assertEqual(parser[REQUEST_KEY][0][0], 0)
        self.assertEqual(parser[REQUEST_KEY][0][1]['pid'], '12256')

    def test_parse_log_line(self):
        """Assert _parse_log_line returns a true result for a valid line."""
        parser = UwsgiLogDict()
        result = parser._parse_log_line(0, EXAMPLE_LOG_LINE)
        self.assertTrue(result)

    def test_parse_log_line_bad(self):
        """Assert _parse_log_line returns a false result for an invalid line."""
        parser = UwsgiLogDict()
        result = parser._parse_log_line(0, 'not a real line')
        self.assertFalse(result)

    def test_parse_master_line(self):
        """Assert the master process line is found and parses."""
        parser = UwsgiLogDict()
        result = parser._parse_log_line(0, EXAMPLE_MASTER_LINE)
        self.assertTrue(result)
        self.assertEqual(parser['master'][0][1]['pid'], '10614')

    @patch('django_stacker.fab_modules.uwsgi.fetch_process_dict')
    def test_uwsgi_worker_status(self, uwsgi_fetch_process_dict):
        """Assert dictionary with multiple entires returns only the proper."""
        process_dict = {'worker_id': 1, 'pid': 4365, 'cores': 3,
                        'status': 'running'}
        test_log_dict = {WORKER_KEY: [[0, process_dict]]}
        uwsgi_fetch_process_dict.return_value = process_dict
        result = uwsgi_worker_processes(test_log_dict)
        self.assertEqual(result, [process_dict])


TEST_INI = r"""
[uwsgi]
chdir = /gigtracker
chmod-socket = 666
daemonize = /var/log/uwsgi/gigtracker.log
gid = uwsgi
master = True
max-requests = 5000
module = gigtracker.wsgi:application
pidfile = /tmp/gigtracker.pid
procname = uwsgi_gigtracker
setenv DJANGO_SETTINGS_MODULE=gigtracker.settings;
socket = /tmp/gigtracker.sock
stats = 0.0.0.0:9091
threads = 3
touch-reload = /gigtracker/gigtracker/system/uwsgi.ini
uid = uwsgi
vacuum = True
virtualenv = /home/uwsgi/venv/
workers = 5
"""


class TestUwsgiINI(TestCase):
    """Assert INI files are pulled and parsed."""

    @patch('django_stacker.fab_modules.uwsgi.exists')
    @patch('django_stacker.fab_modules.uwsgi.get')
    @patch('django_stacker.fab_modules.uwsgi.UwsgiINI._parse')
    def test_uwsgiini_object_instantiates(self, uwsgi_ini_parse, uwsgi_get,
                                          uwsgi_exists):
        """Assert instatiation calls get and parse."""
        uwsgi_exists.return_value = True
        uwsgi_ini = UwsgiINI()
        uwsgi_exists.is_called_once()
        uwsgi_get.is_called_once()
        uwsgi_ini_parse.is_called_once()

    def test_ini_parses_properly(self):
        """Assert UwsgiINI object calls parse properly."""
        string_io = StringIO(TEST_INI)
        string_io.seek(0)
        uwsgi_ini = UwsgiINI(string_io)
        self.assertEqual(uwsgi_ini['chmod-socket'], '666')


class TestUwsgiManager(TestCase):
    """Test a manager object for easier access."""

    @patch('django_stacker.fab_modules.uwsgi.UwsgiManager.get_log')
    @patch('django_stacker.fab_modules.uwsgi.UwsgiManager._get_ini')
    def test_uwsgi_manager_instantiates(self, get_ini, get_log):
        """Assert the object instantiates."""
        uwsgi_manager = UwsgiManager()
        get_ini.is_called_once()

    @patch('django_stacker.fab_modules.uwsgi.UwsgiINI')
    def test_uwsgi_manager_get_ini(self, uwsgi_uwsgiini):
        """Assert get_log calles the proper object."""
        # string_io = StringIO(TEST_INI)
        # string_io.seek(0)
        # uwsgi_ini = UwsgiINI(string_io)
        uwsgi_uwsgiini.is_called_once()

    @patch('django_stacker.fab_modules.uwsgi.exists')
    @patch('django_stacker.fab_modules.uwsgi.get')
    @patch('django_stacker.fab_modules.uwsgi.UwsgiINI')
    def test_get_log_calls_gete_properly_and_passes_to_object(self, uwsgi_ini,
                                                              uwsgi_get,
                                                              uwsgi_exists):
        """Assert exists, get, and UwsgLogDict are called and .log is set."""
        test_value = 'monty'
        uwsgi_ini.return_value = {}
        uwsgi_manager = UwsgiManager()
        uwsgi_manager.ini[DAEMONIZE] = test_value
        uwsgi_manager.get_log()
        uwsgi_get.assert_called_once_with(test_value, ANY, use_sudo=True)
        uwsgi_exists.assert_called_once_with(test_value)

    @patch('django_stacker.fab_modules.uwsgi.ps_aux')
    @patch('django_stacker.fab_modules.uwsgi.quiet')
    @patch('django_stacker.fab_modules.uwsgi.UwsgiINI')
    def test_uwsgi_processes_returns_process_list(self, uwsgi_ini, uwsgi_quiet,
                                                  process_ps_aux):
        """Assert uwsgi_processes calls ps_aux and checks counts."""
        test_value = 'monty'
        uwsgi_ini.return_value = {}
        uwsgi_manager = UwsgiManager()
        uwsgi_manager.ini[WORKERS] = 0
        test_proc_name = 'python'
        uwsgi_manager.ini[PROCNAME] = test_proc_name
        test_processes = [{CMDLINE: [test_proc_name],}]
        process_ps_aux.return_value = test_processes
        result = uwsgi_manager._uwsgi_processes()
        self.assertEqual(result, test_processes)
        uwsgi_quiet.assert_called_once_with()

    @patch('django_stacker.fab_modules.uwsgi.settings')
    @patch('django_stacker.fab_modules.uwsgi.sudo')
    @patch('django_stacker.fab_modules.uwsgi.UwsgiINI')
    @patch('django_stacker.fab_modules.uwsgi.UwsgiManager._uwsgi_processes')
    def test_start_uwsgi_checks_processes_and_uwses_sudo(self, uwsgi_processes,
                                                         uwsgi_ini, uwsgi_sudo,
                                                         uwsgi_settings):
        """Assert star calles processes twice, settings, and sudo once."""
        test_value = 'monty'
        uwsgi_ini.return_value = {UID: test_value}
        uwsgi_manager = UwsgiManager()
        uwsgi_processes.return_value = False

        uwsgi_manager.start()
        uwsgi_processes.assert_has_calls((), ())
        uwsgi_settings.assert_called_once_with(sudo_user=test_value)
        uwsgi_sudo.assert_called_once_with(
            '/home/uwsgi/venv/bin/uwsgi --ini '
            '/gigtracker/gigtracker/system/uwsgi.ini')

    @patch('django_stacker.fab_modules.uwsgi.settings')
    @patch('django_stacker.fab_modules.uwsgi.sudo')
    @patch('django_stacker.fab_modules.uwsgi.UwsgiINI')
    @patch('django_stacker.fab_modules.uwsgi.UwsgiManager._uwsgi_processes')
    def test_stop_uwsgi_checks_processes_and_uwses_sudo(self, uwsgi_processes,
                                                        uwsgi_ini, uwsgi_sudo,
                                                        uwsgi_settings):
        """Assert star calles processes twice, settings, and sudo once."""
        test_value = 'monty'
        uwsgi_ini.return_value = {PIDFILE: test_value}
        uwsgi_manager = UwsgiManager()
        uwsgi_processes.return_value = True

        result = uwsgi_manager.stop()
        uwsgi_processes.assert_has_calls((), ())
        uwsgi_sudo.assert_called_once_with(
            '/home/uwsgi/venv/bin/uwsgi --stop monty')
        self.assertFalse(result)
