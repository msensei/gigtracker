import datetime
import json
import logging
from collections import namedtuple

from fabric.api import local, run
from fabric.context_managers import quiet
from fabric.operations import sudo
from tabulate import tabulate

logger = logging.getLogger(__name__)

NameIOwnership = namedtuple('NameIOwnership',
                            ['null', 'type', 'group', 'user', 'name'])


def local_uname():
    local('hostname')


def remote_uname():
    run('uname -a')


def owner(pathname):
    """Determine the owner of a folder and return.

    :param str pathname: The path to the item to check with namei.
    :return str: The  name of the user who owns the pathname.
    """
    owning_user = run(
        """python -c 'import os,pwd;print pwd.getpwuid(os.stat(
        "/var/log/uwsgi/gigtracker.log").st_uid).pw_name'""")
    log_entry = '%s is owned by %s' % (pathname, owning_user)
    logger.info(log_entry)
    return owning_user


def chown(pathname, username, groupname, use_sudo=True):
    """Change the owner of a pathname.

    :param str pathname: The path to the item.
    :param str username: The user's name.
    :param str groupname: The group's name.
    :return: None. (Consider boolean and a non-catestrophic fail state).
    """
    with quiet():
        command = 'chown -R {username}:{groupname} {pathname}'.format(
            username=username, groupname=groupname, pathname=pathname)
        result = sudo(command) if use_sudo else run(command)
        log_entry = '%s is owned by %s' % (pathname, username)
        logger.info(log_entry)


def chmod(permissions, pathname, use_sudo=True):
    """Change the owner of a pathname.

    :param str permissions: Any form of the chmod permissions scheme.
    :param str pathname: The path to the item.
    :return: None. (Consider boolean and a non-catestrophic fail state).
    """
    with quiet():
        command = 'chmod -R {permissions} {pathname}'.format(
            permissions=permissions, pathname=pathname)
        result = sudo(command) if use_sudo else run(command)
        log_entry = '%s has permissions %s' % (pathname, permissions)
        logger.info(log_entry)


def dict_to_text_table(single_dictionary):
    """
    Output the dictionary in a nice readable fashion in the logs.
    :param dict single_dictionary: A single dictionary.
    :param list keys: An optional list of keys to use.
    :returns:
    """
    return '\n' + tabulate([single_dictionary], headers='keys')


def list_of_dict_to_text_table(list_of_dicts):
    """
    Output the dictionary in a nice readable fashion in the logs.
    :param dict single_dictionary: A single dictionary.
    :param list keys: An optional list of keys to use.
    :returns:
    """
    return '\n' + tabulate(list_of_dicts, headers='keys')


def pretty_list_dict(list_of_dicts, header_list):
    """Rather than lettering tabulate guess. This """
    return tabulate(
        [[item[key] for key in header_list] for item in list_of_dicts],
        headers=header_list)


def datetime_from_timestamp(floating_utc_string):
    """Standard timestamp used by psutil."""
    return datetime.datetime.fromtimestamp(floating_utc_string).strftime(
        "%Y-%m-%d %H:%M:%S")


def python_c(python_string):
    """The Python to execute on the remote system.

    :param python_string:
    :return str: The result of the python call as a string.
    """
    python_command = 'python -c "%s"' % python_string
    result = run(python_command)
    if result.return_code == 0:
        return result

def string_to_python(python_string):
    """Use the JSON library to convert the string to a python object."""
    result = """[{'0': """ + python_string + '}]'
    cleaned_result = (str(result)).replace("'", "\"")
    result_as_list = json.loads(cleaned_result)
    return result_as_list[0]['0']
