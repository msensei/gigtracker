"""uWSGI Tools."""
import ConfigParser
import logging
import re
import time
from StringIO import StringIO
from collections import Counter

from django.conf import settings as django_settings
from fabric.context_managers import settings, quiet
from fabric.contrib.files import exists
from fabric.operations import get, sudo

from django_stacker.fab_modules.common import dict_to_text_table, \
    list_of_dict_to_text_table
from django_stacker.fab_modules.constants import MASTER_KEY, PID, \
    REQUEST_KEY, STATUS, WORKER_KEY, WORKER_ID,  \
    CMDLINE, DATETIME, UWSGI, PROCNAME, WORKERS, PIDFILE, \
    DAEMONIZE, UID
from django_stacker.fab_modules.process import fetch_process_dict, \
    ps_aux
from django_stacker.fab_modules.publish import pull_migrate_and_collect

UWSGI_APP = django_settings.UWSGI_APP
UWSGI_INI = django_settings.UWSGI_INI
logger = logging.getLogger(__name__)

REQUEST_KEY_EXPRESSION = (
    r'\[pid:\s(?P<pid>.*?)\|app:\s(?P<app>.*?)\|req:\s(?P<req>.*?)\] ('
    r'?P<ip_address>.*?) \(\)\s{(?P<vars>.*?) vars in (?P<var_bytes>.*?) '
    r'bytes}\ '
    r'\[(?P<datetime>.*?)\]\ (?P<request_method>POST|GET|DELETE|PUT)\s('
    r'?P<request_uri>[^ ]*?) =>\ generated\s(?P<bytes>.*?)\sbytes\ in\ ('
    r'?P<resp_msecs>\d+)\ msecs\s\(HTTP\/[\d.]+\ (?P<resp_status>\d+)\)\s('
    r'?P<headers>.*)\sheaders in\s(?P<header_bytes>.*)\sbytes\s\(('
    r'?P<switches>.*)\sswitches on core\s(?P<core>.*)\)')

EXPRESSION_DICT = {
    MASTER_KEY: r""".*?master process \(pid:\s(?P<pid>.*?)\)""",
    REQUEST_KEY: REQUEST_KEY_EXPRESSION,
    WORKER_KEY: r'.*?spawned uWSGI worker (?P<worker_id>.*?) \(pid: ('
                r'?P<pid>.*?), cores: (?P<cores>.*)\)',
}


class UwsgiManager(object):
    """
    Creates and handles transactions of a uWSGI instances on remote servers.
    """
    UWSGI_ONLINE_MESSAGE = 'uWSGI ONLINE'

    def __init__(self):
        self._get_ini()

    def _get_ini(self):
        self.ini = UwsgiINI()

    def get_log(self):
        string_buffer = StringIO()
        uwsgi_log_path = self.ini[DAEMONIZE]
        if exists(uwsgi_log_path):
            get(uwsgi_log_path, string_buffer, use_sudo=True)
            self.log = UwsgiLogDict(string_buffer)

    def _uwsgi_processes(self):
        """Returns the uWSGI processes or [] if offline.

        :returns list: [{pid='1',...}, ...]"""
        with quiet():
            processes = ps_aux()
        uwsgi_processes = []
        for process in processes:
            if (process[CMDLINE] and
                        process[CMDLINE][0] == self.ini[PROCNAME]):
                uwsgi_processes.append(process)
        if not len(uwsgi_processes) == int(self.ini[WORKERS]) + 1:
            logger.debug('uWSGI MISSING WORKERS')
            return []
        if uwsgi_processes:
            logger.info(self.UWSGI_ONLINE_MESSAGE)
        return uwsgi_processes

    def start(self):
        """Start the uWSGI process on the destination server."""
        if self._uwsgi_processes():
            logger.info('uWSGI ALREADY RUNNING')
            return True
        with settings(sudo_user=self.ini[UID]):
            sudo('%s --ini %s' % (UWSGI_APP, UWSGI_INI))
        if self._uwsgi_processes():
            logger.info(self.UWSGI_ONLINE_MESSAGE)
            return True
        return False

    def stop(self):
        """
        Stop uWSGI.
        :param uwsgi_path: The virtual environment used.
        :param str pid_file: The .pid file  used
        """
        if not self._uwsgi_processes():
            logger.debug('uWSGI OFFLINE (already)')
            return True
        sudo('%s --stop %s' % (UWSGI_APP, self.ini[PIDFILE]))
        if self._uwsgi_processes():
            logger.warn('Unable to stop uWSGI')
            return False
        else:
            logger.info('uWSGI OFFLINE')
            return True

    def status(self):
        """Display the status of the server and some statistics."""
        return bool(self._uwsgi_processes())

    def refresh(self):
        """Reset the remote destination, pull, collect, touch, verify."""
        original_process_list = self._uwsgi_processes()
        original_pids = [int(process[PID]) for process in original_process_list]
        pull_migrate_and_collect()
        sudo('touch %s' % (UWSGI_INI))
        time.sleep(3)
        new_process_list = self._uwsgi_processes()
        new_pids = [int(process[PID]) for process in new_process_list]
        if len(set(new_pids).intersection(original_pids)) != 1:
            logger.error('uWSGI did not refresh.')
            return False
        return True

    def print_requests(self):
        """Pretty Print requests."""
        self.get_log()
        dicts = [item[1] for item in self.log[REQUEST_KEY]]
        print list_of_dict_to_text_table(dicts)

    def aggregations(self):
        """Pull some aggregations from the logs."""
        self.get_log()
        requests = self.log[REQUEST_KEY]
        oldest_date = requests[0][1][DATETIME]
        newest_date = requests[-1][1][DATETIME]
        request_count = len(requests)
        response_times = [int(request[1]['resp_msecs']) for request in requests]
        avg_response_time = sum(response_times, 0.0) / len(response_times)
        urls = Counter([request[1]['request_uri'] for request in requests])
        result = {'oldest_date': oldest_date,
                  'newest_date': newest_date,
                  'request_count': request_count,
                  'avg_response_time': avg_response_time,
                  'urls': urls}
        logger.info(dict_to_text_table(result))
        return result


class UwsgiINI(dict):
    """An dict of the uWSGI Config.

    :param StringIO ini_string_io: A string IO object to use instead of fetch.
    """
    MISSING_ERROR = 'uWSGI INI MISSING'

    def __init__(self, ini_string_io=None):
        if ini_string_io:
            self._ini_string_io = ini_string_io
        else:
            self._ini_string_io = StringIO()
            if not self._get_ini():
                raise ImportError(self.MISSING_ERROR)
        self._config = ConfigParser.RawConfigParser(allow_no_value=True)
        self._parse()

    def _get_ini(self):
        """Get the INI from the remote installation."""
        if not exists(UWSGI_INI):
            logger.error(self.MISSING_ERROR)
            return False
        get(UWSGI_INI, self._ini_string_io, use_sudo=True)
        self._ini_string_io.seek(0)
        self._ini_string_buffer = self._ini_string_io
        return True

    def _parse(self):
        """Take the ini file and parse it onto this object.

        :raises ConfigParser.NoSectionError: If the uwsgi.ini is missing [uwsgi]
        """
        self._config.readfp(self._ini_string_io)
        self._items = self._config.items(UWSGI)
        for key, value in self._items:
            self[key] = value
        logger.debug(str(self))


class UwsgiLogDict(dict):
    """A dictionary created from parts of a uWSGI log file.

    Usage:
        UwsgiLogDict(StringIO)
        -or-
        UwsgiLogDict().parse_file(StringIO)

    :param StringIO: The data stream of a uWSGI log file.
    """

    def __init__(self, string_io=None):
        super(UwsgiLogDict, self).__init__()
        self.expression_dict = {}
        for key, value in EXPRESSION_DICT.iteritems():
            self[key] = []
            self.expression_dict[key] = re.compile(value)
        if string_io:
            self.parse_file(string_io)

    def _parse_log_line(self, line_number, line_value):
        """
        Try to parse the line, else put it in the unknown set.
        :param str line: A line of a file.
        """
        for key, re_compiler in self.expression_dict.iteritems():
            matched_log_line = re_compiler.search(line_value)
            if matched_log_line:
                line_dict = matched_log_line.groupdict()
                self[key].append([line_number, line_dict])
                return True
        return False

    def parse_file(self, string_io):
        """
        Parse an entire 'file' (StringIO) for known lines.
        :param StringIO string_io: Input log 'file.'
        :return: Dictionary of all known results.
        """
        string_io.seek(0)
        line_list = string_io.readlines()
        for line_number, line_value in enumerate(line_list):
            self._parse_log_line(line_number, line_value)


def uwsgi_master_process_dict(uwsgi_log_dict):
    """
    What is the master process (by log).
    :param dict uwsgi_log_dict: A dictionary of the current log.
    :return dict: The dictionary of the pid or None if there is no process.
    """
    last_master = uwsgi_log_dict[MASTER_KEY][-1][1]
    last_master_pid = last_master[PID]
    log_message = 'Last Known uWSGI Master PID: %s' % last_master_pid
    logger.debug(log_message)
    process_dict = fetch_process_dict(last_master_pid)
    if process_dict:
        master_status = process_dict[STATUS]
        log_message = 'uWSGI Master Status: %s' % master_status
        logger.debug(log_message)
        return process_dict
    return None


def uwsgi_worker_processes(uwsgi_log_dict):
    """
    What is the status of the current uWSGI child processes (by log).
    :param dict uwsgi_log_dict: A dictionary of the current log.
    :return list: list of process dictionaries.
    """
    workers = uwsgi_log_dict[WORKER_KEY]
    last_workers = []
    while workers:
        last_worker = workers.pop()[1]
        last_workers.append(last_worker)
        if last_worker[WORKER_ID] == '1':
            break
    process_list = []
    for worker in last_workers:
        process_dict = fetch_process_dict(worker[PID])
        if process_dict:
            process_status = process_dict[STATUS]
            process_list.append(process_dict)
            log_message = 'uWSGI Child Status: %s' % process_status
            logger.debug(log_message)
    return process_list


def processes_from_log(uwsgi_log_dict):
    """Fetch the uwsgi log file and then parse for the processes."""
    valid_statuses = ['sleeping', 'running']
    master_process = uwsgi_master_process_dict(uwsgi_log_dict)
    uwsgi_master_online = False
    uwsgi_workers_online = False
    if master_process:
        if master_process[STATUS] in valid_statuses:
            uwsgi_master_online = True
            workers = uwsgi_worker_processes(uwsgi_log_dict)
            worker_status = [worker[STATUS] for worker in workers]
            if worker_status:
                if (set(worker_status).difference(set(valid_statuses)) ==
                        set([])):
                    uwsgi_workers_online = True
                else:
                    log_message = 'uWSGI Worker ERROR: %s' % worker_status
                    logger.debug(log_message)
            else:
                log_message = 'uWSGI Worker NOT RUNNING'
                logger.debug(log_message)
        else:
            log_message = 'uWSGI Master ERROR: %s' % master_process[STATUS]
            logger.error(log_message)
    else:
        log_message = 'uWSGI Master NOT RUNNING'
        logger.debug(log_message)
    if uwsgi_master_online and uwsgi_workers_online:
        logger.info('uWSGI ONLINE (by log)')
        return True
    logger.info('uWSGI OFFLINE')
    return False
