from StringIO import StringIO

from fabric.operations import get

REMOTE_APP_HOME = '/gigtracker'


def fetch_logs():
    remote_path = '/var/log/uwsgi/gigtracker.log'
    string_buffer = StringIO()
    get(remote_path, string_buffer)
    string_buffer.seek(0)
    log_list = string_buffer.readlines()
    while log_list:
        print(log_list.pop(0))
