from fabric.context_managers import cd, prefix
from fabric.context_managers import settings
from fabric.operations import sudo
from django.conf import settings as django_settings

UWSGI_USER = django_settings.UWSGI_USER
UWSGI_DJANGO_ROOT = django_settings.UWSGI_DJANGO_ROOT
UWSGI_VIRTENV = django_settings.UWSGI_VIRTENV

# TODO: Prep logs
# TODO: deal with virtenv
# TODO: "manage.py"
# TODO: tests once i'm sure I like the methods.


def pull_migrate_and_collect():
    """Resets the current install location, pull, collectstatic."""
    git_pull()
    migrate()
    collect_static()


def git_pull():
    """Pull the GIT repo to the proper location."""
    with settings(sudo_user=UWSGI_USER):
        with cd(UWSGI_DJANGO_ROOT):
            with prefix('source %sbin/activate' % UWSGI_VIRTENV):
                sudo('git reset --hard')
                sudo('git checkout master')
                sudo('git pull')


def migrate():
    """Run migrate across the database instance."""
    # TODO: If there is more than one host. This could be problematic.
    # TODO: May be problematic if server still running.
    with settings(sudo_user=UWSGI_USER):
        with cd(UWSGI_DJANGO_ROOT):
            with prefix('source %sbin/activate' % UWSGI_VIRTENV):
                sudo('python manage.py migrate --noinput')


def collect_static():
    """Run collect static."""
    with settings(sudo_user=UWSGI_USER):
        with cd(UWSGI_DJANGO_ROOT):
            with prefix('source %sbin/activate' % UWSGI_VIRTENV):
                sudo('python manage.py collectstatic --noinput')



#
# def refresh(touch='True', pull='True'):
#     """Update the site, pulling and then restarting uWSGI.
#
#     :param bool touch: use touch when updating, else call uwsgi.
#     :param bool pull: use git.pull, else just restart.
#     """
#     if not exists(MAIN_LOG_FOLDER):
#         sudo('mkdir %s' % MAIN_LOG_FOLDER)
#         sudo('chmod 777 %s' % MAIN_LOG_FOLDER)
#     chown(REMOTE_APP_HOME, UWSGI_USER, UWSGI_USER)
#     chmod('777', REMOTE_APP_HOME)
#     if strtobool(pull):
#         git.pull(REMOTE_APP_HOME, user=UWSGI_USER)
#     with settings(sudo_user=UWSGI_USER):
#         with cd(REMOTE_APP_HOME):
#             with prefix('source /home/uwsgi/venv/bin/activate'):
#                 sudo('python manage.py collectstatic --noinput')
#     if strtobool(touch):
#         sudo('touch %s' % (UWSGI_INI))
#     else:
#         sudo(
#             '%s/bin/uwsgi --ini %s/uwsgi.ini' %
#             (VIRTENV, REMOTE_APP_SYSTEM))
#
#
# def deploy():
#     with cd(REMOTE_DESTINATION):
#         sudo('git checkout -- .')
#     make_working_copy()
#     destructive_restart_script = (REMOTE_SYSTEM_FOLDER +
#                                   '/destructive_restart.sh')
#     sudo('chmod a+x %s' % destructive_restart_script)
#     sudo(destructive_restart_script)
#
#
# def make_working_copy():
#     # sudo('rm -rf %s' % REMOTE_DESTINATION)
#     # run('git config --list')
#     path = REMOTE_DESTINATION
#     branch = 'system_basics'
#     update = 'true'
#     use_sudo = True
#     # user
#     working_copy(remote_url=GIT_URL, path=path, branch=branch,
#                  update=update, use_sudo=True)
