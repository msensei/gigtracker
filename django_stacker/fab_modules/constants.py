CMDLINE = 'cmdline'
MASTER_KEY = 'master'
PID = 'pid'
PROCESS_ATTRIBUTE_LIST = ['pid', 'name', 'username', 'status', 'create_time',
                          'num_threads', 'cpu_percent', 'memory_percent',
                          'cmdline', ]
REQUEST_KEY = 'request'
STATUS = 'status'
UNKNOWN_KEY = 'unknown'
UTC_DATETIME_FIELDS = ['create_time', ]
VIRTUALENV = 'virtualenv'
WORKER_KEY = 'worker'
WORKER_ID = 'worker_id'
DATETIME = 'datetime'
UWSGI = 'uwsgi'
PROCNAME = 'procname'
WORKERS = 'workers'
PIDFILE = 'pidfile'
UID = 'uid'
DAEMONIZE = 'daemonize'
