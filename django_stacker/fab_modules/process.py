import json
import logging
from collections import namedtuple

from fabric.context_managers import quiet
from fabric.operations import run

from django_stacker.fab_modules.constants import CMDLINE
from django_stacker.fab_modules.constants import PROCESS_ATTRIBUTE_LIST, \
    UTC_DATETIME_FIELDS
from django_stacker.fab_modules.common import dict_to_text_table, \
    datetime_from_timestamp, python_c, string_to_python

logger = logging.getLogger(__name__)

Process = namedtuple('Process', PROCESS_ATTRIBUTE_LIST)


def fetch_process_dict(pid):
    """
    Fetch a Process dictionary from host via pid.
    :param int pid: Integer of the process id.
    :return dict: A dictionary of process values or None if no process.
    """
    fetch_process_info_command = (
        'from psutil import Process;print Process(%s).as_dict(attrs=%s);') % (
            pid,
            PROCESS_ATTRIBUTE_LIST)
    with quiet():
        result = python_c(fetch_process_info_command)
    if result:
        result_json_string = (str(result)).replace("'", "\"")
        result_as_dict = json.loads(result_json_string)
        clean_process_results(result_as_dict)
        log_output = dict_to_text_table(result_as_dict)
        logger.debug(log_output)
        return result_as_dict
    log_output = 'PROCESS PID:%s NOT FOUND' % pid
    logger.debug(log_output)
    return None


def clean_process_results(result_as_dict):
    """
    Clean designated fields of the results.
    :param dict result_as_dict: A dictionary of results from Process.
    """
    for key in UTC_DATETIME_FIELDS:
        result_as_dict[key] = datetime_from_timestamp(result_as_dict[key])
    result_as_dict[CMDLINE] = result_as_dict[CMDLINE][0]


def process_list():
    result = run(
        r"""python -c 'import psutil;print [[proc.pid, proc.name(),
        proc.cmdline()] for proc in psutil.process_iter()];'""")
    result = """[{'0': """ + result + '}]'
    result_json_string = (str(result)).replace("'", "\"")
    result_as_dict = json.loads(result_json_string)
    result_list = result_as_dict[0]['0']
    clean_list = [item[2][0] for item in result_list if item and item[2]]
    logger.debug(clean_list)
    return clean_list


def ps_aux():
    """Returns a list of tuples about the processes running on the machine.

    The initiating Python command does not JSON serialize, so we remove it.

    :returns list: [(PID, name, cmdline),...]
    """
    command = ('import psutil;'
               'print [proc.as_dict(attrs=%s) for proc in '
               'psutil.process_iter()];') % str(PROCESS_ATTRIBUTE_LIST)
    result = python_c(command)
    result = result.replace(command, u'FAB COMMAND')
    if result:
        python_result = string_to_python(result)
        for item in python_result:
            for key in UTC_DATETIME_FIELDS:
                item[key] = datetime_from_timestamp(item[key])
        return python_result
    return []
