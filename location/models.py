from __future__ import unicode_literals

from django.db import models

from common.constants import STANDARD_CHARFIELD_LENGTH


# TODO: use django-location-field


class Location(models.Model):
    city = models.CharField(max_length=STANDARD_CHARFIELD_LENGTH)
