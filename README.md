## Synopsis

A Python Django package for tracking gigs and rating them.
Morphed into a uWSGI remote process manager.
App will split shortly.

uWSGI Manager is intended to be a stand alone django addon when complete.

## Code Example

python manage.py runserver
127.0.0.1:8000
serves a typical Django project with nice classes and some tests.
/admin works for all models at the moment.

fab ps_aux

Available commands:

    ps_aux         Display the dictionary of remote processes.
    uwsgi_refresh  Pull the desired branch to the server, touch restart, pid ...
    uwsgi_start    Start uWSGI on the remote server(s).
    uwsgi_stats    A brief synopsis of the existing log file.
    uwsgi_status   A check for "up" using the expected remote process details...
    uwsgi_stop     Stop uWSGI on the remote server(s).

(python_27_django) C:\Users\BIT\PycharmProjects\gigtracker>fab uwsgi_refresh

[54.183.195.66] Executing task 'uwsgi_refresh'
[54.183.195.66] download: <file obj> <- /gigtracker/gigtracker/system/uwsgi.ini
2016-03-10 08:48:27,101 - fab - _uwsgi_processes - INFO - uWSGI ONLINE
[54.183.195.66] sudo: git reset --hard
[54.183.195.66] out: HEAD is now at 3f6cc72 Merged in system_basics (pull request #1)
[54.183.195.66] sudo: git checkout master
[54.183.195.66] out: Already on 'master'
[54.183.195.66] out: Your branch is up-to-date with 'origin/master'.
[54.183.195.66] sudo: python manage.py collectstatic --noinput
[54.183.195.66] out: 0 static files copied to '/gigtracker/static', 82 unmodified.
[54.183.195.66] sudo: touch /gigtracker/gigtracker/system/uwsgi.ini
2016-03-10 08:48:31,966 - fab - _uwsgi_processes - INFO - uWSGI ONLINE

Done.
Disconnecting from 54.183.195.66... done.

## Motivation

I need to track all these gigs better when looking, and I think I could
scrape a bunch of the data I need to 'cut out the middle man.'

I want a really clean public app to reference when lecturing and teaching,
I also am tired of doing the same tasks over and over,
so I'm automating my life. Feel free to help.

## Installation

git clone the repo
change the IP, PEM, and user in fab_modules/base_env.py # TODO: settings.py

## Tests

Tests automatically generate JUNIT xml for CI systems.
manage.py test
