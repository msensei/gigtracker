import inspect

from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from django.db.models.base import ModelBase

from tag import models

for name, obj in inspect.getmembers(models):
  if inspect.isclass(obj):
    if isinstance(obj, ModelBase):
      if not obj._meta.abstract:  # pylint: disable=protected-access
        try:
          admin.site.register(obj)
        except AlreadyRegistered:
          pass
