from __future__ import unicode_literals

from django.db import models

from common.constants import STANDARD_DECIMAL_MAX_DIGITS, \
    STANDARD_DECIMAL_PLACES
from common.models import NameMixin, DescriptionMixin, Timeframe


class BenefitPackage(NameMixin, DescriptionMixin):
    pass


class Benefit(NameMixin, DescriptionMixin):
    pass


class BenefitCost(models.Model):
    benefit = models.ForeignKey(Benefit)
    cost = models.DecimalField(max_digits=STANDARD_DECIMAL_MAX_DIGITS,
                               decimal_places=STANDARD_DECIMAL_PLACES)
    timeframe = models.ForeignKey(Timeframe)

